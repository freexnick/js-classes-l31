class Character {
  name = "";
  role = "Normie";
  health = Math.floor(Math.random() * 1000);
  armor = Math.floor(Math.random() * 100);
  damageAmount = Math.floor(Math.random() * 200);
  damageType = "Physical Damage";

  constructor(name = "Anonymous") {
    this.name = name;
  }

  health() {
    console.log(`${this.name}, your current health is ${this.health}`);
    return this;
  }
  armor() {
    console.log(`${this.name}, your current armor is ${this.armor}`);
    return this;
  }
  damage() {
    console.log(`${this.name} deals ${this.damageAmount} ${this.damageType}`);
    return this;
  }
}

class Mage extends Character {
  role = "Mage";
  damageType = "Magic Damage";
  #magicTypes = [
    "Conjuration",
    "Abjuration",
    "Divination",
    "Evocation",
    "Necromancy",
    "Transmutation",
    "Illusion",
    "Enchantment",
  ];
  magicPower = this.#magicTypes[
    Math.floor(Math.random() * this.#magicTypes.length)
  ];

  constructor(name) {
    super(name);
  }

  fly() {
    console.log(`${this.name} starts levitating`);
    return this;
  }
}

class Support extends Mage {
  role = "Support";
  healAmount = Math.floor(Math.random() * 150);

  heal() {
    console.log(`${this.name} heals for ${this.healAmount}`);
    return this;
  }
}

class Adc extends Character {
  role = "Adc";
  attackDamage = Math.floor(Math.random() * 100);
  attackRange = Math.floor(Math.random() * 100);

  constructor(name) {
    super(name);
  }

  range() {
    if (this.attackRange > 30) {
      console.log(
        `${this.name}, your attack range is ${this.attackRange}, perfect for ranged attacks`
      );
      return this;
    } else {
      console.log(
        `${this.name}, your attack range is ${this.attackRange}, might as well  reroll as tank`
      );
      return this;
    }
  }
}
